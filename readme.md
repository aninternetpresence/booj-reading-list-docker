# Setup

The site's default hostname is `brl.aninternetpresence.net`. If you'd like to run the site on a different hostname, create a `laravel/env_conf/environment.conf` file that defines the site name. It only needs
to have contents like the following:

    Define servername brl.mysite.local

This would make the site available with domain name `brl.mysite.local`
    
Setup a web server on your host machine to proxy requests to the docker containers. Run the containers with forwarded ports so that they can listen on alternative connections. 
Here is an example of building the `booj-reading-list` container with the laravel site and the linked `booj-mariadb` container which will have the database:

    cd ~/booj-reading-list-docker/laravel
    
    docker build -t enderandpeter/booj-reading-list .
    
    cd ~/booj-reading-list-docker/mariadb
    
    docker build -t enderandpeter/booj-mariadb .
    
    # Use either an automatically set alternative port with the -P option, or specific ports with -p
    
    # Start the DB container
    
    docker run -P --name booj-mariadb --restart=always -d enderandpeter/booj-mariadb
    
    # Link a volume for ssl certificates to /etc/ssl/ in the container if using certificates
    
    docker run -p 8080:80 -p 8443:443 -v ~/www/booj-reading-list/:/var/www/laravel/ -v /etc/ssl:/etc/ssl/ -d --name=booj-reading-list enderandpeter/booj-reading-list
    
The laravel container is based on [eboraas/laravel](https://hub.docker.com/r/eboraas/laravel/) and the mariadb container is the official one.

After the containers are setup, you can configure the web server on the host OS to redirect requests to the laravel container. First, find the IP of the docker container, either the host IP if the host is Linux or the VM's IP as shown in the `DOCKER_HOST` environment variable after running `eval $(docker-machine env)`. Then, redirect requests to this address:

    <VirtualHost *:80>
      ServerName brl.mysite.local
     
      ProxyRequests Off
      ProxyVia Block
      ProxyPreserveHost On
     
      <Proxy *>
        Require all granted
      </Proxy>
    
      ProxyPass / https://73.x.x.x:8080/
	  ProxyPassReverse / https://73.x.x.x:8080/
    </VirtualHost>
   
  
If using SSL, you can redirect requests to the host OS on port 80 to the 443 virtualhost:
    
    Listen 443 # If not already listening on this port
    
    <VirtualHost *:80>
        ServerName brl.mysite.local
        Redirect permanent / https://brl.mysite.local:443/
    </VirtualHost>


    <VirtualHost *:443>
      ServerName brl.mysite.local
   
      SSLProxyEngine On   
      ProxyRequests Off
      ProxyVia Block
      ProxyPreserveHost On
     
      <Proxy *>
        Require all granted
      </Proxy>
    
      ProxyPass / https://73.x.x.x:8443/
	  ProxyPassReverse / https://73.x.x.x:8443/
    </VirtualHost>

## Tips for Linux host
If you are running Docker directly on Ubuntu, then be sure to use the host OS's IP for the `ProxyPass` directives
    
## Tips for Windows host
If your host OS is Windows, you'll need to use forward slashes for file paths and use a prefix like `//{driver_letter}/`, e.g. `//c/Users/Documents/booj-reading-list`.

When accessing the laravel container through a command like `docker exec -it booj-reading-list bash`, the command must be prepended with `winpty` when using a terminal like `msysgit` or any other MinGW or Cygwin tool. This will allow the terminal to properly interpret the TTY connection.

You will also want to set the TERM variable to `cygwin` in the bash shell for laravel's Ubuntu guest OS  so that you can use normal shell commands and navigation in the container.

