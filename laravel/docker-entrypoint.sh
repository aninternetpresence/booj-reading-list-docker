#!/bin/bash
set -e

# Symlink the Storage file location to the public folder
if [ ! -L /var/www/laravel/public/storage ]; then
    ln -s /var/www/laravel/storage/app/public /var/www/laravel/public/storage
fi

exec "$@"